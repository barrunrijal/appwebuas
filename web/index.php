<?php
include 'koneksi.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Mobil Paramitha</title>
  </head>
  <body>
  <!--Navbar-->
  <nav class="navbar navbar-expand-lg navbar-light bg-warning fixed-top">
  <a class="navbar-brand" href="#">Mobil Paramitha</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
  <!--Navbar end-->
  <!--Jumbotron-->
  <div class="jumbotron jumbotron-fluid " style="background-color:orange;">
  <div class="container text-center">
    <h1 class="display-4"></h1>
	<img src="p1.png" height=25% width=25%>
    <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
  </div>
</div>
  <!--jumbotron end-->
  <!-- About -->
<div class= "container text-center mt-5">
<span id='About'></span>
	<h1><b>About</b></h1>
  <div class="row text-justify">
    <div class="col-sm">
     Politeknik Negeri Malang alias Kampus Baru kini terus berbenah. Hal yang paling kentara adalah penyelesaian gedung-gedung besar seperti Graha Polinema dan Gedung Kuliah Bersama. Belum lagi tahun 2018 Polinema mulai membangun hanggar pesawat untuk keperluan perkuliahan. Terdapat juga penambahan sarana prasana untuk kenyamanan mahasiswa. Namun diluar pembaruan yang kasat mata, Polinema juga berbenah di sisi akademis dan sistem. Tahun 2017 terdapat penambahan jumlah minggu, yang tentunya juga menyesuaikan ketentuan Kementerian Riset, Teknologi dan Perguruan Tinggi (Kemenristekdikti). Ditambah adanya sistem Kartu Rencana Studi (KRS) bagi mahasiswa, meskipun dalam prosesnya tak sama dengan yang ada di universitas.
	 Tak kalah santer diperbincangkan akhir-akhir ini adalah mengenai isu munculnya Program Studi (prodi) baru D3 Pertambangan dan bergabungnya Politeknik Kediri menjadi Program Studi di Luar Kampus Utama (PSDKU) dibawah naungan Polinema.
    </div>
    <div class="col-sm">
      Politeknik Kediri kini berubah menjadi PSDKU Polinema. Merunut dari Permenristekdikti Nomor 1 tahun 2017, PSDKU adalah program studi yang diselenggarakan di kabupaten/kota/kota administratif yang tidak berbatasan langsung dengan Kampus Utama. PSDKU dapat dibuka di provinsi yang sama dengan provinsi letak Kampus Utama berada, atau provinsi yang berbeda dengan provinsi dimana Kampus Utama berada. Berbeda dengan PDD, ketika sudah PSDKU maka perguruan tinggi memiliki status yang sama dengan kampus utama.
    </div>
  </div>
  </div>
  <!--About end-->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>